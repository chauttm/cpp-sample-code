#include <iostream>
using namespace std;

int main()
{
    string user_name;
    int number = 19;
    int guess;

    cout << "Hello! What is your name?" << endl;
    cin >> user_name;
    cout << "Play with me, " << user_name << "! I am thinking of a number between 1 and 1000." << endl;
    cout << "Take a guess. " << endl;
    cin >> guess;
    cout << "Your guess is " << guess <<". We'll continue next time.";

    return 0;
}
