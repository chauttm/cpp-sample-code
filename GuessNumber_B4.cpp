#include <iostream>
using namespace std;

int main()
{
    string user_name;
    int number = 19;
    int guess;
    bool won = false;

    cout << "Hello! What is your name?" << endl;
    cin >> user_name;
    cout << "Play with me, " << user_name << "! I am thinking of a number between 1 and 1000." << endl;
    cout << "Take a guess. " << endl;
    cin >> guess;

    won = guess == number;
    if (won)
        cout << "Excellent! You guessed my number!";
    else if (guess > number)
        cout << "Your guess is too high.";
    else
        cout << "Your guess is too low.";

    return 0;
}
