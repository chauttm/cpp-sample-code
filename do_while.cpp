#include <iostream>
using namespace std;

int main()
{
    int number;
    cin >> number;
    do {
        cout << number << " ";	// bước 1
        number--;		        // bước 2
    } while (number > 0);		// bước 3
    cout << "Boom!";	        // bước 4
}
